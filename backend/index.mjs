import Fastify from 'fastify';
import auth from '@fastify/auth';
import compress from '@fastify/compress';
import { jwtVerify, SignJWT, } from 'jose';
import cookie from '@fastify/cookie';
import cors from '@fastify/cors';
import { MongoClient, ServerApiVersion, ObjectId, } from 'mongodb';
import { hash, verify, } from 'argon2';
import { randomUUID } from 'crypto';

const client = new MongoClient('mongodb://db:27017', {
    serverApi: {
        version: ServerApiVersion.v1,
        strict: true,
        deprecationErrors: true,
    },
});

const db = client.db('uptimer');
const links = db.collection('links');
const entries = db.collection('entries');
const users = db.collection('users');
const incidents = db.collection('incidents');

await users.createIndex({
    login: 1,
}, {
    unique: true,
});

const fastify = Fastify({
    logger: true,
});

function isConnectionSecure(request) {
    return (
        request.raw.socket?.encrypted === true ||
        request.headers['x-forwarded-proto'] === 'https'
    )
}

fastify.register(compress);
fastify.register(cookie);
fastify.register(auth);
fastify.register(cors, {
    origin: async () => true,
    credentials: true,
});

const alg = 'HS256';
const secret = (new TextEncoder()).encode('3XwX4fPbwhW4EbfAnCuMX!jGt3B^QZA^');

fastify.decorateRequest('jwt', undefined);

fastify.decorate('signJwt', async function (data, expiration) {
    const signer = new SignJWT(data);
    signer.setExpirationTime(expiration);
    signer.setProtectedHeader({ alg });

    return await signer.sign(secret);
});

fastify.decorate('verifyJwt', async function (request, reply) {
    const jwt = request.cookies?.jwt;

    if (jwt === undefined) {
        throw {
            error: 'Missing JWT token',
        };
    }

    try {
        const { payload } = await jwtVerify(jwt, secret);

        request.jwt = payload;
    } catch {
        throw {
            error: 'JWT signature mismatch',
        };
    }
});

await fastify.after();

fastify.post('/login', {
    schema: {
        body: {
            type: 'object',
            properties: {
                login: { type: 'string', },
                password: { type: 'string', },
            },
            required: [
                'login',
                'password',
            ],
        },
    },
}, async (request, reply) => {
    const passwordsMatch = await (async () => {
        if (process.env.ENABLE_ADMIN_ACCESS && request.body.login === 'admin' && request.body.password === 'admin') {
            return true;
        }

        const found = await users.findOne({
            login: request.body.login,
        }, {
            projection: {
                _id: 0,
                password: 1,
            },
        });

        if (found === null) {
            return false;
        }

        return await verify(found.password, request.body.password);
    })();

    if (!passwordsMatch) {
        reply.status(401);

        return {
            error: 'Incorrect login or password',
        };
    }

    reply.cookie('jwt', await fastify.signJwt({
        login: request.body.login,
    }, '15m'), {
        signed: false,
        secure: isConnectionSecure(request),
        sameSite: 'strict',
        maxAge: 60 * 15,
        httpOnly: true,
        path: '/',
    });

    return {};
});

fastify.get('/logout', async (request, reply) => {
    reply.clearCookie('jwt');

    return {};
});

fastify.get('/session', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
}, async (request, reply) => {
    return request.jwt;
});

fastify.get('/links', async (request, reply) => {
    return await links.find({}).toArray();
});

fastify.put('/links', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                title: { type: 'string', minLength: 1, },
                href: { type: 'string', minLength: 1, },
            },
            required: [
                'title',
                'href',
            ],
        },
    },
}, async (request, reply) => {
    const { title, href } = request.body;

    try {
        new URL(href);
    } catch {
        reply.code(400);

        return { error: 'Invalid URL' };
    }

    const result = await links.insertOne({
        title,
        href,
    });

    reply.code(201);

    return {
        _id: result.insertedId,
    };
});

fastify.delete('/links', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                _id: { type: 'string', minLength: 1, },
            },
            required: [
                '_id',
            ],
        },
    },
}, async (request, reply) => {
    await links.deleteOne({
        _id: new ObjectId(request.body._id),
    });

    return {};
});

fastify.patch('/links', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                _id: { type: 'string', minLength: 1, },
                title: { type: 'string', minLength: 1, },
                href: { type: 'string', minLength: 1, },
            },
            required: [
                '_id',
            ],
        },
    },
}, async (request, reply) => {
    const { _id, title, href, } = request.body;

    try {
        new URL(href);
    } catch {
        reply.code(400);

        return { error: 'Invalid URL' };
    }

    await links.updateOne({
        _id: new ObjectId(_id),
    }, {
        $set: {
            title,
            href,
        },
    });

    return {};
});

fastify.get('/entries', async (request, reply) => {
    return await entries.find({}).toArray();
});

fastify.delete('/entries', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                _id: { type: 'string', minLength: 1, },
            },
            required: [
                '_id',
            ],
        },
    },
}, async (request, reply) => {
    await entries.deleteOne({
        _id: new ObjectId(request.body._id),
    });

    return {};
});

fastify.put('/entries', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                name: { type: 'string', minLength: 1, },
                status: { type: 'string', minLength: 1, },
            },
            required: [
                'name',
                'status',
            ],
        },
    },
}, async (request, reply) => {
    const { name, status, } = request.body;

    const result = await entries.insertOne({
        name,
        status,
    });

    reply.code(201);

    return {
        _id: result.insertedId,
    };
});

fastify.patch('/entries', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                _id: { type: 'string', minLength: 1, },
                name: { type: 'string', minLength: 1, },
                status: { type: 'string', minLength: 1, },
            },
            required: [
                '_id',
            ],
        },
    },
}, async (request, reply) => {
    const { _id, name, status, } = request.body;

    await entries.updateOne({
        _id: new ObjectId(_id),
    }, {
        $set: {
            name,
            status,
        },
    });

    return {};
});

// TODO: permissions
fastify.get('/users', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
}, async (request, reply) => {
    return await users.find({}, {
        projection: { _id: 0, login: 1, },
    }).toArray();
});

fastify.put('/users', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                login: { type: 'string', minLength: 1, },
            },
            required: [
                'login',
            ],
        },
    },
}, async (request, reply) => {
    const password = randomUUID();

    try {
        const result = await users.insertOne({
            login: request.body.login,
            password: await hash(password),
        });
    
        return {
            password,
        };   
    } catch (error) {
        if (error.code === 11000) {
            reply.code(400);

            return { error: 'User already exists' };
        }

        throw error;
    }
});

fastify.post('/users', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                action: { type: 'string', minLength: 1, },
                login: { type: 'string', minLength: 1, },
            },
            required: [
                'action',
                'login',
            ],
        },
    },
}, async (request, reply) => {
    if (request.body.action === 'resetPassword') {
        const password = randomUUID();

        const result = await users.updateOne({
            login: request.body.login,
        }, {
            $set: {
                password: await hash(password),
            }
        });

        return { password };
    }

    reply.code(400);
    return { error: 'Unkown action' };
});

fastify.patch('/users', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                fromLogin: { type: 'string', minLength: 1, },
                toLogin: { type: 'string', minLength: 1, },
            },
            required: [
                'fromLogin',
                'toLogin',
            ],
        },
    },
}, async (request, reply) => {
    await users.updateOne({
        login: request.body.fromLogin,
    }, {
        $set: {
            login: request.body.toLogin,
        },
    });

    return {};
});

fastify.delete('/users', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                login: { type: 'string', minLength: 1, },
            },
            required: [
                'login',
            ],
        },
    },
}, async (request, reply) => {
    await users.deleteOne({
        login: request.body.login,
    });

    return {};
});

fastify.get('/incidents', async (request, reply) => {
    const current = await incidents.find({
        resolved: false,
    }).toArray();

    return current;
});

fastify.get('/incidents/:id', {
    schema: {
        params: {
            type: 'object',
            properties: {
                id: { type: 'string', minLength: 1, },
            },
            required: [
                'id',
            ],
        },
    },
}, async (request, reply) => {
    const result = await incidents.findOne({
        _id: new ObjectId(request.params.id),
    });

    if (result === null) {
        reply.code(404);
        return {};
    }

    return result;
});

fastify.put('/incidents', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                name: { type: 'string', minLength: 1, },
                message: { type: 'string', minLength: 1, },
            },
            required: [
                'name',
                'message',
            ],
        },
    },
}, async (request, reply) => {
    const date = new Date();

    const result = await incidents.insertOne({
        date,
        resolved: false,
        name: request.body.name,
        history: [
            {
                type: 'Investigating',
                message: request.body.message,
                date,
            },
        ],
    });

    return { _id: result.insertedId, };
});

fastify.patch('/incidents', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                _id: { type: 'string', minLength: 1, },
                patch: {
                    type: 'object',
                    properties: {
                        _id: { type: 'string', minLength: 1, },
                        date: {
                            type: 'string',
                            format: 'date-time',
                        },
                        resolved: { type: 'boolean', },
                        name: { type: 'string', minLength: 1, },
                        history: {
                            type: 'array',
                            items: {
                                type: 'object',
                                properties: {
                                    type: { type: 'string', minLength: 1, },
                                    message: { type: 'string', minLength: 1, },
                                    date: {
                                        type: 'string',
                                        format: 'date-time',
                                    },
                                },
                                required: [
                                    'type',
                                    'message',
                                    'date',
                                ],
                            },
                            minItems: 1,
                        },
                    },
                    required: [
                        'date',
                        'resolved',
                        'name',
                        'history',
                    ],
                },
            },
            required: [
                '_id',
                'patch',
            ],
        },
    },
}, async (request, reply) => {
    delete request.body.patch?._id;

    const { patch } = request.body;

    patch.date = new Date(patch.date);

    for (let i = 0; i < patch.history; i++) {
        const date = patch.history[i].date;

	    patch.history[i].date = new Date(date);
    }

    await incidents.updateOne({
        _id: new ObjectId(request.body._id),
    }, {
        $set: patch,
    });

    return {};
});

fastify.delete('/incidents', {
    onRequest: fastify.auth([
        fastify.verifyJwt,
    ]),
    schema: {
        body: {
            type: 'object',
            properties: {
                _id: { type: 'string', minLength: 1, },
            },
            required: [
                '_id',
            ],
        },
    },
}, async (request, reply) => {
    await incidents.deleteOne({
        _id: new ObjectId(request.body._id),
    });

    return {};
});

fastify.get('/', async (request, reply) => {
    let start = new Date();
    start.setUTCHours(0, 0, 0, 0);
    
    let end = new Date();
    end.setUTCHours(23, 59, 59, 999);
    
    const current = await incidents.find({
        $or: [
            {
                date: {
                    $gte: start,
                    $lt: end,
                },
            },
            {
                resolved: false,
            },
        ],
    }).toArray();

    return {
        entries: await entries.find({}).toArray(),
        links: await links.find({}).toArray(),
        current,
    };
});

try {
    await fastify.listen({ port: 3001, host: '0.0.0.0', });
} catch (err) {
    fastify.log.error(err);
}
