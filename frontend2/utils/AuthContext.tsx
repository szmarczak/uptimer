import type React from 'react';
import { createContext, useState, useEffect } from 'react';
import { useRouter } from 'next/navigation';
import { getSession } from './fetch';

export const AuthContext = createContext<{
    session: any;
    setSession: React.Dispatch<React.SetStateAction<any>>;
}>(undefined as never);

export function AuthContextProvider({ children, loggedOutPath = '/', fallback }: { children: React.ReactNode }) {
    const [session, setSession] = useState<any>(undefined);
    const router = useRouter();

    // TODO: log out when the session expires

    useEffect(() => {
        const controller = new AbortController();
        const { signal } = controller;

        (async () => {
            try {
                setSession(await getSession(signal));
            } catch {
                if (signal.aborted) {
                    return;
                }

                if (typeof loggedOutPath === 'string') {
                    router.push(loggedOutPath);
                }
            }
        })();

        return () => {
            controller.abort();
        };
    }, []);

    return (
        <AuthContext.Provider value={{ session, setSession }}>
            {session && children}
            {!session && fallback}
        </AuthContext.Provider>
    );
};
