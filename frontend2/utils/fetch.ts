export const json = async (url: string, options?: Omit<RequestInit, 'body'> & { body?: any }, parentUrl?: string) => {
    const resolved = new URL(url, parentUrl);

    options = {...options};
    options.mode = 'cors';
    options.credentials = 'include';
    options.headers = {...options.headers};

    if ('content-type' in options.headers) {
        throw new Error('Unexpected \'content-type\' header');
    }

    // @ts-expect-error TS gets it wrong.
    options.headers['content-type'] = 'application/json';

    if ('body' in options) {
        options.body = JSON.stringify(options.body);
    }

    const response = await fetch(resolved, options);

    return {
        __proto__: null,
        status: response.status,
        json: await response.json().catch(() => undefined),
        headers: response.headers,
        redirected: response.redirected,
        ok: response.ok,
        type: response.type,
        url: response.url,
    };
};

export const logIn = async (login: string, password: string) => {
    const response = await json('/login', {
        method: 'POST',
        body: {
            login,
            password,
        },
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw new Error(`${response.status} - ${response.json?.error}`);
    }
};

export const getSession = async (signal?: AbortSignal) => {
    const response = await json('/session', {
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw new Error(`${response.status} - ${response.json?.error}`);
    }

    return response.json;
};

export const getLinks = async (signal?: AbortSignal) => {
    const response = await json('/links', {
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const putLink = async (title: string, href: string, signal?: AbortSignal) => {
    const response = await json('/links', {
        method: 'PUT',
        signal,
        body: {
            title,
            href,
        },
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const deleteLink = async (_id: string, signal?: AbortSignal) => {
    const response = await json('/links', {
        method: 'DELETE',
        signal,
        body: {
            _id,
        },
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const patchLink = async (_id: string, title: string, href: string, signal?: AbortSignal) => {
    const response = await json('/links', {
        method: 'PATCH',
        signal,
        body: {
            _id,
            title,
            href,
        },
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const getEntries = async (signal?: AbortSignal) => {
    const response = await json('/entries', {
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const deleteEntry = async (_id: string, signal?: AbortSignal) => {
    const response = await json('/entries', {
        method: 'DELETE',
        body: { _id, },
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const putEntry = async (name: string, status: string, signal?: AbortSignal) => {
    const response = await json('/entries', {
        method: 'PUT',
        body: { name, status, },
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const patchEntry = async (_id: string, name: string, status: string, signal?: AbortSignal) => {
    const response = await json('/entries', {
        method: 'PATCH',
        signal,
        body: {
            _id,
            name,
            status,
        },
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const getUsers = async (signal?: AbortSignal) => {
    const response = await json('/users', {
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const putUser = async (login: string, signal?: AbortSignal) => {
    const response = await json('/users', {
        method: 'PUT',
        signal,
        body: {
            login,
        },
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response;
    }

    return response.json;
};

export const postUser = async (body: any, signal?: AbortSignal) => {
    const response = await json('/users', {
        method: 'POST',
        signal,
        body,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const patchUser = async (fromLogin: string, toLogin: string, signal?: AbortSignal) => {
    const response = await json('/users', {
        method: 'PATCH',
        signal,
        body: {
            fromLogin,
            toLogin,
        },
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response;
    }

    return response.json;
};

export const deleteUser = async (login: string, signal?: AbortSignal) => {
    const response = await json('/users', {
        method: 'DELETE',
        body: { login, },
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response.status;
    }

    return response.json;
};

export const createIncident = async (name: string, message: string, signal?: AbortSignal) => {
    const response = await json('/incidents', {
        method: 'PUT',
        body: { name, message, },
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response;
    }

    return response.json;
};

export const patchIncident = async (_id: string, patch: any, signal?: AbortSignal) => {
    const response = await json('/incidents', {
        method: 'PATCH',
        body: { _id, patch, },
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response;
    }

    return response.json;
};

export const getIncident = async (_id: string, signal?: AbortSiganl) => {
    const response = await json(`/incidents/${_id}`, {
        method: 'GET',
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response;
    }

    return response.json;
};

export const deleteIncident = async (_id: string, signal?: AbortSignal) => {
    const response = await json('/incidents', {
        method: 'DELETE',
        body: { _id, },
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response;
    }

    return response.json;
};

export const getIncidents = async (signal?: AbortSignal) => {
    const response = await json('/incidents', {
        signal,
    }, process.env.NEXT_PUBLIC_BACKEND_URL);

    if (!response.ok || response.json === undefined) {
        throw response;
    }

    return response.json;
};
