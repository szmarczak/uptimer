let last = 0;
const ids = new WeakMap();

export default function id(object) {
    if (ids.has(object)) {
        return ids.get(object);
    } else {
        const id = `${String(last++)}-${Math.random().toString(36)}`;
        ids.set(object, id);

        return id;
    }
};
