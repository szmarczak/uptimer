const f = async (url: string, options?: RequestInit) => {
    const resolved = new URL(url, process.env.BACKEND_URL);
    const response = await fetch(resolved, options);

    return await response.json();
};

export const getHomepageData = async (): Promise<{
    entries: {
        name: string,
        operational: boolean,
        outage: boolean,
        down: boolean,
    }[],
    links: {
        title: string,
        href: string,
    }[],
}> => f('', {
    next: {
        revalidate: 10
    }
});
