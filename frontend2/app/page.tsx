import Image from 'next/image';
import Link from 'next/link';
import { getHomepageData } from '@/utils/repo';
import { cache, Fragment } from 'react';
import id from '@/utils/id';

const colors = {
    fill: {
        operational: 'fill-emerald-600',
        outage: 'fill-amber-600',
        down: 'fill-rose-600',
    },
    bg: {
        operational: 'bg-emerald-600',
        outage: 'bg-amber-600',
        down: 'bg-rose-600',
    },
} as const;

const texts = {
    operational: 'Operational',
    outage: 'Outage',
    down: 'Down',

    all: {
        operational: 'All Systems Operational',
        outage: 'Partial Outage',
        down: 'All Systems Down',
    },
};

type Entry = {
    name: string,
    status: 'operational' | 'outage' | 'down',
};

const getGlobalStatus = cache((entries: Entry[]) => {
    let operationalCount = 0;
    let outageCount = 0;
    let downCount = 0;

    for (const entry of entries) {
        if (entry.status === 'operational') {
            operationalCount++;
        } else if (entry.status === 'outage') {
            outageCount++;
        } else if (entry.status === 'down') {
            downCount++;
        }
    }

    if (outageCount !== 0 || (operationalCount !== 0 && downCount !== 0)) {
        return 'outage';
    } else if (operationalCount === 0) {
        return 'down';
    } else {
        return 'operational';
    }
});

export default async function Home() {
    const { links, entries, current } = await getHomepageData();
    const globalStatus = await getGlobalStatus(entries);

    return (
        <>
        <nav className="container mx-auto p-4">
            <ul className="flex gap-2">
                <li className="text-2xl">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M464 256A208 208 0 1 1 48 256a208 208 0 1 1 416 0zM0 256a256 256 0 1 0 512 0A256 256 0 1 0 0 256zM232 120V256c0 8 4 15.5 10.7 20l96 64c11 7.4 25.9 4.4 33.3-6.7s4.4-25.9-6.7-33.3L280 243.2V120c0-13.3-10.7-24-24-24s-24 10.7-24 24z"/></svg>
                    <Link prefetch={false} href="/" className="ml-1 font-bold">uptimer</Link>
                </li>
                <li className="grow"></li>
                {links.map((link) => <li key={link.title}><Link prefetch={false} href={link.href} className="hover:underline">{link.title}</Link></li>)}
            </ul>
        </nav>
        <main className="container mx-auto p-4">
            <article className={`${colors.bg[globalStatus]} p-4 text-white font-bold rounded`}>
                <span className="fill-white">
                    {globalStatus === 'operational' && <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M438.6 105.4c12.5 12.5 12.5 32.8 0 45.3l-256 256c-12.5 12.5-32.8 12.5-45.3 0l-128-128c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0L160 338.7 393.4 105.4c12.5-12.5 32.8-12.5 45.3 0z"/></svg>}
                    {globalStatus === 'outage' && <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 32c14.2 0 27.3 7.5 34.5 19.8l216 368c7.3 12.4 7.3 27.7 .2 40.1S486.3 480 472 480H40c-14.3 0-27.6-7.7-34.7-20.1s-7-27.8 .2-40.1l216-368C228.7 39.5 241.8 32 256 32zm0 128c-13.3 0-24 10.7-24 24V296c0 13.3 10.7 24 24 24s24-10.7 24-24V184c0-13.3-10.7-24-24-24zm32 224a32 32 0 1 0 -64 0 32 32 0 1 0 64 0z"/></svg>}
                    {globalStatus === 'down' && <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path d="M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z"/></svg>}
                </span>
                <span className="ml-1 text-lg align-middle">{texts.all[globalStatus]}</span>
            </article>
        </main>
        <section className="container mx-auto p-4">
            <div className="rounded border border-slate-300 divide-y divide-solid">
                {entries.map((entry) => (
                    <article className="p-4 flex border-slate-300" key={entry.name}>
                        <span className="font-bold">{entry.name}</span>
                        <span className="grow"></span>
                        <span className="mr-1">{texts[entry.status]}</span>
                        <span className={colors.fill[entry.status]}>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512z"/></svg>
                        </span>
                    </article>
                ))}
            </div>
        </section>
        {/* <section className="container mx-auto p-4 text-right">
            <Link prefetch={false} href="#">
                <span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M438.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-160-160c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L338.8 224 32 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l306.7 0L233.4 393.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l160-160z"/></svg>
                </span>
                <span className="ml-1 font-bold">
                    Past Incidents
                </span>
            </Link>
        </section> */}
        {current.map((incident) => (
            <Fragment key={id(incident)}>
                <hr />
                <section className="container mx-auto p-4">
                    <p className="text-xl font-bold">{incident.name}</p>
                    <div className="grid grid-cols-3">
                        {incident.history.map((history) => (
                            <Fragment key={id(history)}>
                                <div>
                                    <p className="py-2 font-bold">{history.type}</p>
                                    <p className="text-slate-600 py-2">{new Date(history.date).toLocaleString()}</p>
                                </div>
                                <p className="col-span-2 py-2">{history.message}</p>
                            </Fragment>
                        )).reverse()}
                    </div>
                </section>
            </Fragment>
        ))}
        </>
    );
};
