'use client';
import type { FormEvent } from 'react';
import { useRef, useState, useCallback, useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/navigation';
import { logIn, getSession } from '@/utils/fetch';

export default function Login() {
    const login = useRef<HTMLInputElement>(null);
    const password = useRef<HTMLInputElement>(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const router = useRouter();
    const [checkingCredentials, setCheckingCredentials] = useState(true);

    const commit = useCallback(async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        setLoading(true);
        setError('');

        try {
            await logIn(login.current!.value, password.current!.value);
            router.push('/admin/');
        } catch (error) {
            if (typeof error === 'object' && error !== null && 'message' in error && typeof error.message === 'string') {
                setError(error.message);
            } else {
                setError('unknown error');
            }

            setLoading(false);
        }
    }, [login, password]);

    useEffect(() => {
        const controller = new AbortController();
        const { signal } = controller;

        (async () => {
            try {
                await getSession(signal);
                router.push('/admin/');
            } catch {
                setCheckingCredentials(false);
                return;
            }
        })();

        return () => {
            controller.abort();
        };
    }, []);

    return (
        <>
            <div className="h-full grid place-content-center gap-1">
                {checkingCredentials && 'Checking credentials...'}
                {!checkingCredentials && (
                    <form className="contents" onSubmit={commit}>
                        <h1 className="text-2xl font-bold text-center">Welcome!</h1>
                        <input ref={login} disabled={loading} required className="p-2 border rounded focus-visible:outline outline-2 outline-offset-1 outline-cyan-500" placeholder="Login" />
                        <input ref={password} disabled={loading} required className="p-2 border rounded focus-visible:outline outline-2 outline-offset-1 outline-cyan-500" placeholder="Password" type="password" />
                        <button disabled={loading} className="bg-indigo-500 disabled:bg-indigo-200 disabled:cursor-not-allowed italic text-white font-bold p-2 rounded focus-visible:outline outline-2 outline-offset-1 outline-slate-500">Log in</button>
                        <span className="text-rose-700 text-center min-h-[2em]">{error}</span>
                    </form>
                )}
            </div>
        </>
    )
};
