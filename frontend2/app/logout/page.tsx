'use client';
import { useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';

export default function Logout() {
    const router = useRouter();
    const [error, setError] = useState('');

    useEffect(() => {
        (async () => {
            try {
                const response = await fetch(new URL('/logout', process.env.NEXT_PUBLIC_BACKEND_URL), {
                    mode: 'cors',
                    credentials: 'include',
                });
    
                if (!response.ok) {
                    throw new Error();
                }
    
                router.push('/');
            } catch {
                setError('There was a problem logging you out. Please refresh the page or clear cookies manually.');
            }
        })();
    }, []);

    return <>{error}</>;
};
