'use client';
import { useState, useContext } from 'react';
import { usePathname } from 'next/navigation';
import Link from 'next/link';
import { AuthContextProvider, AuthContext } from '@/utils/AuthContext';
import * as Toast from '@radix-ui/react-toast';

const ActiveLink = ({ children, href }) => {
    const pathname = usePathname();

    return (
        <Link href={href} className={`${
            pathname === href ? 'bg-slate-100' : ''
        } hover:bg-slate-200 rounded p-2 block focus-visible:outline outline-2 outline-offset-0 outline-black`}>
            {children}
        </Link>
    );
};

const Fallback = () => {
    return (
        <div className="h-full grid place-content-center gap-1">Checking credentials...</div>
    );
};

function AdminLayout({ children }) {
    const { session } = useContext(AuthContext);

    return (
        <div className="flex min-h-full">
            <nav className="border-r p-4 bg-white shrink-0">
                <ul className="flex flex-col gap-1 h-full">
                    <li className="text-center min-h-[2em]">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M464 256A208 208 0 1 1 48 256a208 208 0 1 1 416 0zM0 256a256 256 0 1 0 512 0A256 256 0 1 0 0 256zM232 120V256c0 8 4 15.5 10.7 20l96 64c11 7.4 25.9 4.4 33.3-6.7s4.4-25.9-6.7-33.3L280 243.2V120c0-13.3-10.7-24-24-24s-24 10.7-24 24z"/></svg>
                        <span className="ml-1 align-middle font-bold">
                            <Link href="/admin/">uptimer</Link>
                        </span>
                    </li>
                    <li><ActiveLink href="/admin/new">New Incident</ActiveLink></li>
                    <li><ActiveLink href="/admin/incidents">Manage Incidents</ActiveLink></li>
                    <li><ActiveLink href="/admin/entries">Manage Entries</ActiveLink></li>
                    <li><ActiveLink href="/admin/links">Manage Links</ActiveLink ></li>
                    <li><ActiveLink href="/admin/users">Manage Users</ActiveLink ></li>
                    <li><ActiveLink href="/logout/">Log out</ActiveLink></li>
                    <li className="grow"></li>
                    <li className="text-gray-500">logged in as: {session.login}</li>
                </ul>
            </nav>
            <main className="grow">{children}</main>
        </div>
    );
};

export default function AdminLayoutWithProviders({ children }) {
    return (
        <AuthContextProvider loggedOutPath="/login" fallback={<Fallback></Fallback>}>
            <Toast.Provider swipeDirection="right">
                <AdminLayout>{children}</AdminLayout>
                <Toast.Viewport className="[--viewport-padding:_25px] fixed bottom-0 right-0 flex flex-col p-[var(--viewport-padding)] gap-[10px] w-[390px] max-w-[100vw] m-0 list-none z-[2147483647] outline-none"></Toast.Viewport>
            </Toast.Provider>
        </AuthContextProvider>
    )
};
