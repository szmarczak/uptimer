'use client';
import { useEffect, useState, forwardRef, useCallback, useContext } from 'react';
import { useRouter } from 'next/navigation';
import { getEntries, deleteEntry, putEntry, patchEntry } from '@/utils/fetch';
import { AuthContext } from '@/utils/AuthContext';
import id from '@/utils/id';

const entryStyle = 'p-4 border-slate-300 block hover:bg-slate-200 flex gap-1';

const statuses = {
    operational: ['Operational', 'bg-emerald-600'],
    outage: ['Outage', 'bg-amber-600'],
    down: ['Down', 'bg-rose-400'],
} as const;

const StatusSelect = forwardRef(function StatusSelect({ value, onChange, disabled, className }, ref) {
    return (
        <select disabled={disabled} className={`${statuses[value]?.[1]} ${className ?? ''} p-2 rounded disabled:opacity-70`} value={value} onChange={onChange} ref={ref}>
            {Object.entries(statuses).map(([key, status]) => (
                <option key={key} value={key} className="bg-white">{status[0]}</option>
            ))}
        </select>
    );
});

const EntryEditor = function EntryEditor({ name, status, setName, setStatus, remove, }) {
    const [processing, setProcessing] = useState(false);
    const [rememberedName, setRememberedName] = useState(false);

    const rememberName = useCallback((e) => {
        setRememberedName(e.target.value);
    }, [setRememberedName]);

    const onNameChange = useCallback(async (e) => {
        if (e.target.value === '') {
            e.target.value = name;
            return;
        }

        setProcessing(true);

        try {
            await setName(e.target.value);
        } finally {
            setProcessing(false);
        }
    }, [name, setName, setProcessing]);

    const onStatusChange = useCallback(async (e) => {
        setProcessing(true);

        try {
            await setStatus(e.target.value);
        } finally {
            setProcessing(false);
        }
    }, [setStatus, setProcessing]);

    const onDelete = useCallback(async () => {
        setProcessing(true);

        try {
            await remove();
        } finally {
            setProcessing(false);
        }
    }, [remove, setProcessing]);

    return (
        <div className={entryStyle}>
            <input disabled={processing} className="disabled:opacity-70 p-1 bg-transparent border border-slate-400 disabled:bg-slate-300 rounded" defaultValue={name} onBlur={onNameChange} onFocus={rememberName} />
            <StatusSelect disabled={processing} value={status} onChange={onStatusChange}></StatusSelect>
            <button disabled={processing} className="p-2 bg-rose-500 rounded disabled:opacity-70" onClick={onDelete}>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M135.2 17.7L128 32H32C14.3 32 0 46.3 0 64S14.3 96 32 96H416c17.7 0 32-14.3 32-32s-14.3-32-32-32H320l-7.2-14.3C307.4 6.8 296.3 0 284.2 0H163.8c-12.1 0-23.2 6.8-28.6 17.7zM416 128H32L53.2 467c1.6 25.3 22.6 45 47.9 45H346.9c25.3 0 46.3-19.7 47.9-45L416 128z"/></svg>
            </button>
        </div>
    );
};

export default function Entries() {
    const [entries, setEntries] = useState([]);
    const { session } = useContext(AuthContext);
    const router = useRouter();

    useEffect(() => {
        const controller = new AbortController();
        const { signal } = controller;

        (async () => {
            try {
                setEntries(await getEntries(signal));
            } catch (error) {
                if (signal.aborted) {
                    return;
                }

                if (error === 401) {
                    router.push('/login');
                    return;
                }

                throw error;
            }
        })();

        return () => {
            controller.abort();
        };
    }, []);

    const addNewEntry = useCallback(() => {
        setEntries([
            ...entries,
            { _id: undefined, name: '', status: 'operational', },
        ]);
    }, [entries, setEntries]);

    const remove = useCallback(async (entry) => {
        try {
            if (entry._id !== undefined) {
                await deleteEntry(entry._id);
            }

            setEntries(entries.filter((e) => e !== entry));
        } catch (error) {
            if (error === 401) {
                router.push('/login');
                return;
            }

            throw error;
        }
    }, [entries, setEntries, router]);

    const setStatus = useCallback(async (entry, status) => {
        if (entry._id === undefined) {
            entry.status = status;
            setEntries([...entries]);
            return;
        }

        try {
            await patchEntry(entry._id, entry.name, status);
    
            entry.status = status;
            setEntries([...entries]);
        } catch (error) {
            if (error === 401) {
                router.push('/login');
                return;
            }

            throw error;
        }
    }, [entries, setEntries, router]);

    const setName = useCallback(async (entry, name) => {
        if (entry.name === name) {
            return;
        }

        const doesNotExistYet = entry._id === undefined;

        try {
            if (doesNotExistYet) {
                const { _id, } = await putEntry(name, entry.status);
    
                entry._id = _id;
                entry.name = name;
                setEntries([...entries]);
            } else {
                await patchEntry(entry._id, name, entry.status);
    
                entry.name = name;
                setEntries([...entries]);
            }
        } catch (error) {
            if (error === 401) {
                router.push('/login');
                return;
            }

            throw error;
        }
    }, [entries, setEntries, router]);

    return (
        <div className="h-full p-8 flex flex-col gap-4 container">
            <h1 className="font-bold text-3xl">Manage Entries</h1>

            <div className="rounded border border-slate-300 divide-y divide-solid">
                <button className={`${entryStyle} italic w-full text-center`} onClick={() => addNewEntry()}>Add New Entry</button>
                {entries.map((entry) => (
                    <EntryEditor
                        key={id(entry)}
                        name={entry.name}
                        status={entry.status}
                        setName={(name) => setName(entry, name)}
                        setStatus={(status) => setStatus(entry, status)}
                        remove={() => remove(entry)}
                    ></EntryEditor>
                ))}
            </div>
        </div>
    );
};
