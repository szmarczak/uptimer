'use client';
import { useState, useCallback, useRef, useEffect, } from 'react';
import { useRouter } from 'next/navigation';
import id from '@/utils/id';
import { getLinks, putLink, deleteLink, patchLink, } from '@/utils/fetch';

const entryStyle = 'p-4 border-slate-300 block hover:bg-slate-200 flex gap-1 w-full items-center';

function LinkOverview({ edit, processing, title, href, remove, }) {
    return (
        <div className={entryStyle}>
            <a href={href} target="_blank" className="underline">{title}</a>
            <div className="grow"></div>
            <button onClick={() => edit()} disabled={processing} className="p-2 bg-emerald-500 rounded disabled:opacity-70">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.7 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z"/></svg>
            </button>
            <button onClick={() => remove()} disabled={processing} className="p-2 bg-rose-500 rounded disabled:opacity-70">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M135.2 17.7L128 32H32C14.3 32 0 46.3 0 64S14.3 96 32 96H416c17.7 0 32-14.3 32-32s-14.3-32-32-32H320l-7.2-14.3C307.4 6.8 296.3 0 284.2 0H163.8c-12.1 0-23.2 6.8-28.6 17.7zM416 128H32L53.2 467c1.6 25.3 22.6 45 47.9 45H346.9c25.3 0 46.3-19.7 47.9-45L416 128z"/></svg>
            </button>
        </div>
    );
};

function LinkEditor({ processing, cancel, submit, title, href, }) {
    const titleRef = useRef();
    const urlRef = useRef();

    const onSubmit = useCallback((e) => {
        e.preventDefault();

        submit(titleRef.current.value, urlRef.current.value);
    }, [titleRef, urlRef, submit]);

    return (
        <div className={entryStyle}>
            <form onSubmit={onSubmit} className="contents">
                <input defaultValue={title} disabled={processing} ref={titleRef} placeholder="Title" className="disabled:opacity-70 p-1 bg-transparent border border-slate-400 disabled:bg-slate-300 rounded" required />
                <input defaultValue={href} disabled={processing} ref={urlRef} placeholder="URL" className="disabled:opacity-70 p-1 bg-transparent border border-slate-400 disabled:bg-slate-300 rounded grow" required type="url" />
                <button type="button" onClick={() => cancel()} disabled={processing} className="p-2 bg-orange-500 rounded disabled:opacity-70">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M48.5 224H40c-13.3 0-24-10.7-24-24V72c0-9.7 5.8-18.5 14.8-22.2s19.3-1.7 26.2 5.2L98.6 96.6c87.6-86.5 228.7-86.2 315.8 1c87.5 87.5 87.5 229.3 0 316.8s-229.3 87.5-316.8 0c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0c62.5 62.5 163.8 62.5 226.3 0s62.5-163.8 0-226.3c-62.2-62.2-162.7-62.5-225.3-1L185 183c6.9 6.9 8.9 17.2 5.2 26.2s-12.5 14.8-22.2 14.8H48.5z"/></svg>
                </button>
                <button disabled={processing} className="p-2 bg-blue-500 rounded disabled:opacity-70">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M64 32C28.7 32 0 60.7 0 96V416c0 35.3 28.7 64 64 64H384c35.3 0 64-28.7 64-64V173.3c0-17-6.7-33.3-18.7-45.3L352 50.7C340 38.7 323.7 32 306.7 32H64zm0 96c0-17.7 14.3-32 32-32H288c17.7 0 32 14.3 32 32v64c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V128zM224 288a64 64 0 1 1 0 128 64 64 0 1 1 0-128z"/></svg>
                </button>
            </form>
        </div>
    );
};

function LinkView({ title, href, remove, upsert, }) {
    const isNew = title === '';

    const [editor, setEditor] = useState(isNew);
    const [processing, setProcessing] = useState(false);
    const router = useRouter();

    const submit = useCallback(async (title, href) => {
        setProcessing(true);

        try {
            await upsert(title, href);

            setEditor(false);
            setProcessing(false);
        } catch (error) {
            if (error === 401) {
                router.push('/login');
            }

            throw error;
        }
    }, [setProcessing, setEditor, router, upsert]);

    const cancel = useCallback(() => {
        setEditor(false);

        if (isNew) {
            remove();
        }
    }, [setEditor, remove]);

    if (editor) {
        return (
            <LinkEditor
                processing={processing}
                cancel={cancel}
                submit={submit}
                title={title}
                href={href}
            ></LinkEditor>
        );
    }

    return (
        <LinkOverview
            edit={() => setEditor(true)}
            remove={remove}
            processing={processing}
            title={title}
            href={href}
        ></LinkOverview>
    );
};

export default function Links() {
    const [links, setLinks] = useState([]);
    const router = useRouter();

    useEffect(() => {
        const controller = new AbortController();
        const { signal } = controller;

        (async () => {
            try {
                setLinks(await getLinks(signal));
            } catch (error) {
                if (signal.aborted) {
                    return;
                }

                if (error === 401) {
                    router.push('/login');
                    return;
                }

                throw error;
            }
        })();

        return () => {
            controller.abort();
        };
    }, []);

    const newLink = useCallback(() => {
        setLinks([
            ...links,
            {
                _id: undefined,
                title: '',
                href: '',
            },
        ]);
    }, [links, setLinks]);

    const remove = useCallback(async (link) => {
        const isNew = link._id === undefined;
        if (isNew) {
            setLinks(links.filter((l) => l !== link));
            return;
        }

        try {
            await deleteLink(link._id);
            setLinks(links.filter((l) => l !== link));
        } catch (error) {
            if (error === 401) {
                router.push('/login');
                return;
            }

            throw error;
        }
    }, [links, setLinks, router]);

    const upsert = useCallback(async (link, title: string, href: string) => {
        try {
            if (link._id === undefined) {
                const { _id } = await putLink(title, href);
    
                link._id = _id;
                link.title = title;
                link.href = href;
                setLinks([...links]);
                return;
            }

            await patchLink(link._id, title, href);
            link.title = title;
            link.href = href;
            setLinks([...links]);
        } catch (error) {
            if (error === 401) {
                router.push('/login');
                return;
            }

            throw error;
        }
    }, [links, setLinks, router]);

    return (
        <div className="h-full p-8 flex flex-col gap-4 container">
            <h1 className="font-bold text-3xl">Manage Links</h1>

            <div className="rounded border border-slate-300 divide-y divide-solid">
                <button onClick={newLink} className={`${entryStyle} italic w-full text-center`}>Add New Link</button>

                {links.map((link) => (
                    <LinkView
                        key={id(link)}
                        title={link.title}
                        href={link.href}
                        remove={() => remove(link)}
                        upsert={(title, href) => upsert(link, title, href)}
                    ></LinkView>
                ))}
            </div>
        </div>
    );
};
