'use client';
import { useRouter } from 'next/navigation';
import { useCallback, useState, useRef } from 'react';
import { createIncident } from '@/utils/fetch';

export default function New() {
    const router = useRouter();
    const nameRef = useRef();
    const messageRef = useRef();
    const [processing, setProcessing] = useState(false);

    const onSubmit = useCallback(async (e) => {
        e.preventDefault();

	setProcessing(true);

        try {
            const { _id } = await createIncident(nameRef.current.value, messageRef.current.value);

            router.push(`/admin/incidents/${_id}`);
        } catch (response) {
            if (response.status === 401) {
                router.push('/login');
                return;
            }

            // TODO: handle this
        } finally {
            setProcessing(false);
        }
    }, [setProcessing]);

    return (
        <div className="h-full p-8 flex flex-col gap-4 container">
            <h1 className="font-bold text-3xl">Create New Incident</h1>

            <form className="flex flex-col gap-2" onSubmit={onSubmit}>
                <input
                    placeholder="What are you investigating?"
                    className="text-2xl disabled:opacity-70 p-1 bg-transparent border border-slate-400 disabled:bg-slate-300 rounded"
                    disabled={processing}
                    ref={nameRef}
                    required={true}
                />
                <textarea
                    placeholder="Describe the action taken."
                    className="disabled:opacity-70 p-1 bg-transparent border border-slate-400 disabled:bg-slate-300 rounded"
                    disabled={processing}
                    ref={messageRef}
                    required={true}
                ></textarea>
                <div>
                    <button disabled={processing} className="rounded bg-gray-300 p-2">Create</button>
                </div>
            </form>
        </div>
    );
};
