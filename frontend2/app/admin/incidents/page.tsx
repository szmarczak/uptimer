'use client';
import { getIncidents } from '@/utils/fetch';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';
import Link from 'next/link';
import id from '@/utils/id';

const entryStyle = 'p-4 border-slate-300 block hover:bg-slate-200 flex gap-1 w-full items-center';

export default function Incidents() {
    const [openIncidents, setOpenIncidents] = useState([]);
    const router = useRouter();

    useEffect(() => {
        const controller = new AbortController();
        const { signal } = controller;

        (async () => {
            try {
                setOpenIncidents(await getIncidents());
            } catch (response) {
                if (signal.isAborted) {
                    return;
                }

                if (response.status === 401) {
                    router.push('/login');
                    return;
                }
    
                throw response;
            }
        })();

        return () => {
            controller.abort();
        };
    }, []);

    return (
        <div className="h-full p-8 flex flex-col gap-4 container">
            <h1 className="font-bold text-3xl">Manage Incidents</h1>
            <h2 className="font-bold text-2xl">Open Incidents</h2>

            <div className="rounded border border-slate-300 divide-y divide-solid">
                {openIncidents.map((incident) => (
                    <Link key={id(incident)} className={entryStyle} href={`/admin/incidents/${incident._id}`}>{incident.name}</Link>
                ))}
            </div>
        </div>
    );
};
