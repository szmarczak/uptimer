'use client';
import { getIncident, patchIncident, deleteIncident, } from '@/utils/fetch';
import { useState, useEffect, useCallback, Fragment } from 'react';
import { useRouter } from 'next/navigation';

export default function Incident({ params }) {
    const router = useRouter();
    const [incident, setIncident] = useState(undefined);
    const [updatedIncident, setUpdatedIncident] = useState(undefined);
    const [processing, setProcessing] = useState(false);

    useEffect(() => {
        const controller = new AbortController();
        const { signal } = controller;

        (async () => {
            try {
                const fetched = await getIncident(params.id, signal);

                setIncident(fetched);
                setUpdatedIncident(structuredClone(fetched));
            } catch (response) {
                if (signal.isAborted) {
                    return;
                }

                if (response.status === 401) {
                    router.push('/login');
                    return;
                }
    
                if (response.status === 404) {
                    router.push('/admin/incidents');
                    return;
                }
    
                throw response;
            }
        })();

        return () => {
            controller.abort();
        };
    }, []);

    const onSubmit = useCallback(async (e) => {
        e.preventDefault();

        setProcessing(true);

        try {
            if (updatedIncident.name === '') {
                updatedIncident.name = incident.name;
            }

            for (let i = 0; i < incident.history.length; i++) {
                if (updatedIncident.history[i].type === '') {
                    updatedIncident.history[i].type = incident.history[i].type;
                }
            }

            await patchIncident(incident._id, updatedIncident);

            setIncident(structuredClone(updatedIncident));
            setUpdatedIncident(structuredClone(updatedIncident));
            setProcessing(false);
        } catch (response) {
            if (response.status === 401) {
                router.push('/login');
                return;
            }

            throw response;
        } finally {
            // setProcessing(false);
        }
    }, [incident, updatedIncident, setIncident, setUpdatedIncident, setProcessing, router]);

    const remove = useCallback(async () => {
        setProcessing(true);

        try {
            await deleteIncident(incident._id);

            router.push('/admin/incidents');
        } catch (response) {
            if (response.status === 401) {
                router.push('/login');
                return;
            }

            throw response;
        } finally {
            // setProcessing(false);
        }
    }, [setProcessing, router, incident]);

    const [updateType, setUpdateType] = useState('');
    const [updateMessage, setUpdateMessage] = useState('');

    const onPushUpdate = useCallback(async (e) => {
        e.preventDefault();

        const clonedIncident = structuredClone(incident);

        clonedIncident.history.push({
            date: new Date().toISOString(),
            type: updateType,
            message: updateMessage,
        });

        setProcessing(true);

        try {
            await patchIncident(incident._id, clonedIncident);

            setIncident(structuredClone(clonedIncident));
            setUpdatedIncident(structuredClone(clonedIncident));

            setUpdateType('');
            setUpdateMessage('');

            setProcessing(false);
        } catch (response) {
            if (response.status === 401) {
                router.push('/login');
                return;
            }

            throw response;
        } finally {
            // setProcessing(false);
        }
    }, [updateType, updateMessage, incident, setProcessing, setUpdateType, setUpdateMessage, router]);

    return (
        <div className="h-full p-8 flex flex-col gap-4 container">
            {incident === undefined && 'Loading...'}
            {incident !== undefined && (
                <>
                    <form onSubmit={onSubmit} className="contents">
                        <input
                            placeholder={incident.name}
                            value={updatedIncident.name}
                            onChange={(e) => {
                                updatedIncident.name = e.target.value;

                                setUpdatedIncident({...updatedIncident});
                            }}
                            className="text-2xl disabled:opacity-70 p-1 bg-transparent border border-slate-400 disabled:bg-slate-300 rounded"
                            disabled={processing}
                        />
                        <label className="flex gap-2">
                            <input
                                type="checkbox"
                                checked={updatedIncident.resolved}
                                onChange={(e) => {
                                    updatedIncident.resolved = e.target.checked;

                                    setUpdatedIncident({...updatedIncident});
                                }}
                            />
                            <span>Resolved?</span>
                        </label>
                        <div className="grid grid-cols-3 gap-2">
                            {updatedIncident.history.map((history, index) => (
                                <Fragment key={index}>
                                    <div>
                                        <input
                                            placeholder={incident.history[index].type}
                                            value={history.type}
                                            onChange={(e) => {
                                                history.type = e.target.value;

                                                setUpdatedIncident({...updatedIncident});
                                            }}
                                            disabled={processing}
                                            className="p-2 font-bold bg-transparent border border-slate-400 rounded disabled:bg-slate-300"
                                        />
                                        <p className="text-slate-600 p-2">{new Date(history.date).toLocaleString()}</p>
                                    </div>
                                    <textarea
                                        required={true}
                                        disabled={processing}
                                        className="col-span-2 p-2 bg-transparent border border-slate-300 disabled:bg-slate-300 rounded"
                                        value={history.message}
                                        onChange={(e) => {
                                            history.message = e.target.value;

                                            setUpdatedIncident({...updatedIncident});
                                        }}
                                    ></textarea>
                                </Fragment>
                            )).reverse()}
                        </div>
                        <div className="flex gap-2">
                            <button disabled={processing} className="rounded bg-gray-300 p-2">Apply changes</button>
                            <button type="button" onClick={remove} disabled={processing} className="rounded bg-rose-500 p-2">Delete</button>
                        </div>
                    </form>
                    <hr />
                    <form className="contents" onSubmit={onPushUpdate}>
                        <p className="text-2xl font-bold">Push an update</p>
                        <div className="grid grid-cols-3">
                            <div>
                                <input
                                    placeholder="Update"
                                    required
                                    value={updateType}
                                    onChange={(e) => {
                                        setUpdateType(e.target.value);
                                    }}
                                    disabled={processing}
                                    className="p-2 font-bold bg-transparent border border-slate-400 rounded disabled:bg-slate-300"
                                />
                            </div>
                            <textarea
                                required={true}
                                disabled={processing}
                                className="col-span-2 p-2 bg-transparent border border-slate-300 disabled:bg-slate-300 rounded"
                                value={updateMessage}
                                onChange={(e) => {
                                    setUpdateMessage(e.target.value);
                                }}
                            ></textarea>
                        </div>
                        <div>
                            <button disabled={processing} className="rounded bg-gray-300 p-2">Publish</button>
                        </div>
                    </form>
                </>
            )}
        </div>
    );
};
