'use client';
import { useRouter, } from 'next/navigation';
import { useState, useCallback, useRef, useEffect, } from 'react';
import { getUsers, putUser, patchUser, deleteUser, postUser } from '@/utils/fetch';
import id from '@/utils/id';
import * as Toast from '@radix-ui/react-toast';

const entryStyle = 'p-4 border-slate-300 block hover:bg-slate-200 flex gap-1 w-full items-center';

function LoginEditor({ processing, cancel, submit, login, }) {
    const loginRef = useRef();

    const onSubmit = useCallback((e) => {
        e.preventDefault();

        submit(loginRef.current.value);
    }, [loginRef, submit]);

    return (
        <div className={entryStyle}>
            <form onSubmit={onSubmit} className="contents">
                <input defaultValue={login} disabled={processing} ref={loginRef} placeholder="Login" className="disabled:opacity-70 p-1 bg-transparent border border-slate-400 disabled:bg-slate-300 rounded" required />
                <div className="grow"></div>
                <button type="button" onClick={() => cancel()} disabled={processing} className="p-2 bg-orange-500 rounded disabled:opacity-70">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M48.5 224H40c-13.3 0-24-10.7-24-24V72c0-9.7 5.8-18.5 14.8-22.2s19.3-1.7 26.2 5.2L98.6 96.6c87.6-86.5 228.7-86.2 315.8 1c87.5 87.5 87.5 229.3 0 316.8s-229.3 87.5-316.8 0c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0c62.5 62.5 163.8 62.5 226.3 0s62.5-163.8 0-226.3c-62.2-62.2-162.7-62.5-225.3-1L185 183c6.9 6.9 8.9 17.2 5.2 26.2s-12.5 14.8-22.2 14.8H48.5z"/></svg>
                </button>
                <button disabled={processing} className="p-2 bg-blue-500 rounded disabled:opacity-70">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M64 32C28.7 32 0 60.7 0 96V416c0 35.3 28.7 64 64 64H384c35.3 0 64-28.7 64-64V173.3c0-17-6.7-33.3-18.7-45.3L352 50.7C340 38.7 323.7 32 306.7 32H64zm0 96c0-17.7 14.3-32 32-32H288c17.7 0 32 14.3 32 32v64c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V128zM224 288a64 64 0 1 1 0 128 64 64 0 1 1 0-128z"/></svg>
                </button>
            </form>
        </div>
    );
};

function UserOverview({ login, processing, remove, submit, reset, password, }) {
    const isNew = login === '';
    const [editing, setEditing] = useState(isNew);

    const cancel = useCallback(() => {
        setEditing(false);

        if (isNew) {
            remove();
        }
    }, [setEditing, remove]);

    const submitAndSwitchLayout = useCallback(async (login) => {
        const result = await submit(login);

        if (result === undefined) {
            setEditing(false);
        }
    }, [submit, setEditing]);

    if (editing) {
        return (
            <LoginEditor
                login={login}
                cancel={cancel}
                submit={submitAndSwitchLayout}
                processing={processing}
            ></LoginEditor>
        );
    }

    return (
        <div className={entryStyle}>
            <span>{login}</span>
            {password && (<input disabled={true} value={password} className="disabled:opacity-70 p-1 bg-transparent border border-slate-400 disabled:bg-slate-300 rounded" />)}
            <div className="grow"></div>
            <button onClick={reset} disabled={processing} className="p-2 bg-gray-500 rounded disabled:opacity-70">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M336 352c97.2 0 176-78.8 176-176S433.2 0 336 0S160 78.8 160 176c0 18.7 2.9 36.8 8.3 53.7L7 391c-4.5 4.5-7 10.6-7 17v80c0 13.3 10.7 24 24 24h80c13.3 0 24-10.7 24-24V448h40c13.3 0 24-10.7 24-24V384h40c6.4 0 12.5-2.5 17-7l33.3-33.3c16.9 5.4 35 8.3 53.7 8.3zM376 96a40 40 0 1 1 0 80 40 40 0 1 1 0-80z"/></svg>
            </button>
            <button onClick={() => setEditing(true)} disabled={processing} className="p-2 bg-emerald-500 rounded disabled:opacity-70">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.7 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z"/></svg>
            </button>
            <button onClick={remove} disabled={processing} className="p-2 bg-rose-500 rounded disabled:opacity-70">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M135.2 17.7L128 32H32C14.3 32 0 46.3 0 64S14.3 96 32 96H416c17.7 0 32-14.3 32-32s-14.3-32-32-32H320l-7.2-14.3C307.4 6.8 296.3 0 284.2 0H163.8c-12.1 0-23.2 6.8-28.6 17.7zM416 128H32L53.2 467c1.6 25.3 22.6 45 47.9 45H346.9c25.3 0 46.3-19.7 47.9-45L416 128z"/></svg>
            </button>
        </div>
    );
};

export default function Users() {
    const [processing, setProcessing] = useState(false);
    const [users, setUsers] = useState([]);
    const router = useRouter();

    const [showToast, setShowToast] = useState(false);
    const [toastTitle, setToastTitle] = useState('');
    const [toastText, setToastText] = useState('');

    useEffect(() => {
        const controller = new AbortController();
        const { signal } = controller;

        (async () => {
            try {
                const fetched = await getUsers(signal);

                setUsers(fetched.map((u) => ({...u, password: ''})));
            } catch (error) {
                if (signal.aborted) {
                    return;
                }

                if (error === 401) {
                    router.push('/login');
                    return;
                }

                throw error;
            }
        })();

        return () => {
            controller.abort();
        };
    }, []);

    const addNewUser = useCallback(() => {
        setUsers([
            ...users,
            {
                login: '',
                password: '',
            },
        ]);
    }, [users, setUsers]);

    const remove = useCallback(async (user) => {
        setProcessing(true);

        try {
            if (user.login !== '') {
                await deleteUser(user.login);
            }

            setUsers(users.filter((u) => u !== user));
        } finally {
            setProcessing(false);
        }
    }, [users, setUsers, setProcessing]);

    const submit = useCallback(async (user, login) => {
        setProcessing(true);

        try {
            if (user.login === '') {
                const { password } = await putUser(login);
                user.login = login;
                user.password = password;
            } else {
                await patchUser(user.login, login);
                user.login = login;
            }

            setUsers([...users]);
        } catch (response) {
            if (response.status === 401) {
                router.push('/login');
                return;
            }

            setToastTitle('Error!');
            setToastText(response.json?.message ?? response.json?.error ?? 'Unknown error');
            setShowToast(true);

            return false;
        } finally {
            setProcessing(false);
        }
    }, [setProcessing, users, setUsers, router, setToastText, setToastText, setShowToast]);

    const resetPassword = useCallback(async (user) => {
        setProcessing(true);

        try {
            const { password } = await postUser({
                login: user.login,
                action: 'resetPassword',
            });

            user.password = password;
            setUsers([...users]);
        } catch (error) {
            if (error === 401) {
                router.push('/login');
                return;
            }

            throw error;
        } finally {
            setProcessing(false);
        }
    }, [users, setUsers, setProcessing, router]);

    return (
        <div className="h-full p-8 flex flex-col gap-4 container">
            <h1 className="font-bold text-3xl">Manage Users</h1>

            <Toast.Root
                className="bg-white rounded-md shadow-[hsl(206_22%_7%_/_35%)_0px_10px_38px_-10px,_hsl(206_22%_7%_/_20%)_0px_10px_20px_-15px] p-[15px] grid [grid-template-areas:_'title_action'_'description_action'] grid-cols-[auto_max-content] gap-x-[15px] items-center data-[state=open]:animate-slideIn data-[state=closed]:animate-hide data-[swipe=move]:translate-x-[var(--radix-toast-swipe-move-x)] data-[swipe=cancel]:translate-x-0 data-[swipe=cancel]:transition-[transform_200ms_ease-out] data-[swipe=end]:animate-swipeOut"
                open={showToast}
                onOpenChange={setShowToast}
            >
                <Toast.Title className="[grid-area:_title] mb-[5px] font-medium text-slate12 text-[15px]">
                    {toastTitle}
                </Toast.Title>
                <Toast.Description asChild>
                    <span
                        className="[grid-area:_description] m-0 text-slate11 text-[13px] leading-[1.3]"
                    >{toastText}</span>
                </Toast.Description>
                <Toast.Close aria-label="Close" className="border border-2 p-1 text-sm bg-rose-50 border-rose-400 rounded">
                    <span aria-hidden>Close</span>
                </Toast.Close>
            </Toast.Root>

            <div className="rounded border border-slate-300 divide-y divide-solid">
                <button onClick={addNewUser} className={`${entryStyle} italic w-full text-center`}>Add New User</button>

                {users.map((user) => (
                    <UserOverview
                        key={id(user)}
                        login={user.login}
                        password={user.password}
                        processing={processing}
                        remove={() => remove(user)}
                        submit={(login) => submit(user, login)}
                        reset={() => resetPassword(user)}
                    ></UserOverview>
                ))}
            </div>
        </div>
    );
};
