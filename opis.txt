Autor: Szymon Marczak S95494

Rendering framework: Next.js (React)
CSS framework: Tailwind

Backend framework: Fastify
Database: MongoDB
MongoDB driver: mongodb (official driver)
Password hash algorithm: argon2
JWT package: jose

