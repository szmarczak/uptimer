FROM node:20.2.0-buster
VOLUME /data
WORKDIR /data
RUN [ "npm", "install", "--ignore-scripts" ]
